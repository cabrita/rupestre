﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType {ore,lifepiece,gem,meat}
public class Collectable : MonoBehaviour
{
    public ItemType type;
    public int meatHeal;

    public string tag_Player = "Player";

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(tag_Player))
        {
            Player_Stats.Instance.item_FX.Play();
            switch (type)
            {
                case ItemType.ore:
                    break;
                case ItemType.lifepiece:
                    Player_Stats.Instance.AddLifePiece();
                    Destroy(gameObject);
                    break;
                case ItemType.meat:
                    Player_Stats.Instance.Heal(meatHeal);
                    Destroy(gameObject);
                    break;
                case ItemType.gem:
                    break;
            }
        }
    }
}
