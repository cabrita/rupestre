﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnTriggerCallEvent : MonoBehaviour
{

	public string targetTag;
	public UnityEvent enterEvent;
	public UnityEvent exitEvent;

	void OnTriggerEnter(Collider _col)
	{
		if (_col.gameObject.CompareTag(targetTag))
		{
			enterEvent.Invoke();
		}
	}

	void OnTriggerExit(Collider _col)
	{
		if (_col.gameObject.CompareTag(targetTag))
		{
			exitEvent.Invoke();
		}
	}
}