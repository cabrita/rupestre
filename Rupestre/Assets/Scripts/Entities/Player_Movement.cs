﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    #region Movement Variables

    public float moveX;
    public float moveY;
    public float y;
    public float walkSpeed;
    [SerializeField] private bool facingright = true;
    public int baseJumpForce;
    public int runningJumpForce;
    public bool playercontrol;
    public int swimSpeed;
    private float previousSpeedx;
    private float previousSpeedy;

    #endregion

    [SerializeField] bool isJumping;
    public int jumps_Max;

    [SerializeField] private bool canCharge;
    [SerializeField] private bool canAttack;
    public bool prepAttack;
    public bool basicAttacking;
    public bool chargedAttacking;
    public bool chargeEffect;
    public int chargePower;
    private float chargingTimer;
    [SerializeField] private bool chargeSlowmo;

    Vector3 mousePos;
    Vector3 spearPos;
    Vector2 my_mouseDir;
    Vector2 spear_mouseDir;
    Vector2 playerPos;

    float angleSpear;
    public bool atkGround;
    public bool canSpearJump;
    public bool spearJumping;
    public float spearJumpx;
    public float spearJumpy;
    private bool aimDown;
    public bool canSpearThrow;
    public float throwForce;
    public bool spearThrowing;
    public bool holdingSpear;

    private BoxCollider2D spear_col;
    private Rigidbody2D spear_Rigidbody;
    private CapsuleCollider2D speartip_col;
    private Animator spearanim;


    public Rigidbody2D my_Rigidbody;
    public Animator my_Animator;
    public SpriteRenderer my_SpriteR;

    //{ get { return feet.onFeet; } }
    [SerializeField] private bool onGround;
    [SerializeField] public bool inWater;
    [SerializeField] private bool onAir;
    [SerializeField] private bool onWall;

    [SerializeField] private bool climbing;
    [SerializeField] private bool aboveVine;
    [SerializeField] private bool belowVine;
    [SerializeField] private bool touchingLatVine;
    [SerializeField] private bool almostFellVine;
    [SerializeField] public int vineJumpforce;
    [SerializeField] private bool lookingOutVine;
    [SerializeField] private bool wasClimbing;
    private bool jumpingBelowVine;
    private bool rightVine;

    Vector3 wallDirection;

    public string tag_Ground;
    public string tag_Water;
    public string tag_Spikes;
    Transform thisSpike;

    public GameObject mesh;
    public GameObject spear;
    public GameObject feet;
    public GameObject head;

    public ParticleSystem water_FX;





    void Start()
    {
        my_Rigidbody = GetComponent<Rigidbody2D>();
        isJumping = false;
        playercontrol = true;
        spear_Rigidbody = spear.GetComponent<Rigidbody2D>();
        spear_col = spear.GetComponent<BoxCollider2D>();
        speartip_col = spear.GetComponent<CapsuleCollider2D>();
        spearanim = spear.GetComponent<Animator>();
        spear_Rigidbody.gravityScale = 0;
        prepAttack = false;

    }

    void Update()
    {

        onGround = Physics2D.Linecast(transform.position, feet.transform.position, 1 << LayerMask.NameToLayer("Ground"));

        if (onGround)
        {
            onAir = false;
            if (!chargedAttacking && !spearJumping)
            {
                canCharge = true;
                canAttack = true;
                canSpearJump = true;
                if (!basicAttacking)
                {
                    canSpearThrow = true;
                }
            }
            spearanim.updateMode = AnimatorUpdateMode.AnimatePhysics;
            my_Rigidbody.drag = 0;
        }
        if (!inWater && !onGround && !climbing)
        {
            onAir = true;
            canSpearJump = false;
        }

        if (climbing)
        {
            onAir = false;
            spearJumping = false;
            canCharge = true;
            my_Rigidbody.drag = 0;
            aboveVine = feet.GetComponent<Player_Feet>().aboveVine;
            if (!isJumping && !chargedAttacking)
            {
                my_Rigidbody.gravityScale = 0;
            }

            //vinha no teto
            if (belowVine)
            {
                if (Input.GetButtonDown("Jump") && !almostFellVine)
                {
                    new WaitForSecondsRealtime(0.1f);
                    my_Rigidbody.AddForce(new Vector2(vineJumpforce * moveX, 0));
                    jumpingBelowVine = true;
                }

                if (Input.GetButtonUp("Jump"))
                {
                    jumpingBelowVine = false;
                }
            }
            if (belowVine && !almostFellVine && !isJumping)
            {
                // se pra baixo para
                if (moveY <= 0 && !touchingLatVine)
                {
                    my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), 0);
                }
                //se pra cima sobe
                if (moveY > 0)
                {
                    my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), (moveY * (walkSpeed * 0.8f)));
                }

            }
            //quase caindo da vinha no teto
            if (almostFellVine && belowVine)
            {
                //voltando pra direita
                if (rightVine && moveX > 0)
                {
                    my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), 0);
                    belowVine = true;
                    almostFellVine = false;
                }
                if (rightVine && moveX < 0)
                {
                    my_Rigidbody.velocity = new Vector2(0, 0);
                }
                //voltando pra esquerda
                if (!rightVine && moveX < 0)
                {
                    belowVine = true;
                    my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), 0);
                    almostFellVine = false;
                }
                if (!rightVine && moveX > 0)
                {
                    my_Rigidbody.velocity = new Vector2(0, 0);
                }
                //vo pulá fodaci
                if (Input.GetButtonDown("Jump"))
                {
                    isJumping = true;
                    belowVine = false;
                    almostFellVine = false;
                    my_Rigidbody.AddForce(new Vector2(0, baseJumpForce + runningJumpForce * Mathf.Abs(moveX * 0.1f)));
                    my_Rigidbody.gravityScale = 1;
                    climbing = false;
                }
            }
            // vinha na parede
            if (!belowVine && touchingLatVine)
            {
                almostFellVine = false;
                //pra dentro é a direita e olhando pra esquerda
                if (rightVine && !facingright)
                {
                    Flip();
                    lookingOutVine = false;
                }
                //pra dentro é a esquerda e olhando pra direita
                if (!rightVine && facingright)
                {
                    Flip();
                    lookingOutVine = false;
                }
                if (!isJumping)
                {
                    // pode se mover pra dentro direita
                    if (rightVine && moveX >= 0)
                    {
                        my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), (moveY * (walkSpeed * 0.8f)));
                        lookingOutVine = false;
                    }
                    // não sair indo pra esquerda e não se mover na vertical
                    if (rightVine && moveX < -0.6f)
                    {
                        my_Rigidbody.velocity = new Vector2(0, 0);
                        lookingOutVine = true;
                    }
                    // não sair indo pra esquerda e poder se mover na vertical
                    if (rightVine && moveX >= -0.6f && moveX < 0)
                    {
                        my_Rigidbody.velocity = new Vector2(0, (moveY * (walkSpeed * 0.8f)));
                        lookingOutVine = false;
                    }
                    // pode se mover pra dentro esquerda
                    if (!rightVine && moveX <= 0)
                    {
                        my_Rigidbody.velocity = new Vector2(moveX * (walkSpeed * 0.8f), (moveY * (walkSpeed * 0.8f)));
                        lookingOutVine = false;
                    }
                    //não sair indo pra direita e poder se mover na vertical
                    if (!rightVine && moveX > 0 && moveX < 0.6f)
                    {
                        my_Rigidbody.velocity = new Vector2(0, (moveY * (walkSpeed * 0.8f)));
                        lookingOutVine = false;
                    }
                    //não sair indo pra direita e não se mover na vertical
                    if (!rightVine && moveX > 0.6f)
                    {
                        my_Rigidbody.velocity = new Vector2(0, 0);
                        lookingOutVine = true;
                    }
                }

                //vamo pular enquanto escala

                if (Input.GetButtonDown("Jump"))
                {
                    if (moveY >= 0.5 && ((!rightVine && moveX <= 0) || (rightVine && moveX >= 0)))
                    {
                        my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, my_Rigidbody.velocity.y * 0.4f);
                    }
                    else
                    {
                        my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, 0);

                    }
                }

                if (isJumping)
                {
                    my_Rigidbody.gravityScale = 1;
                }
            }

        }

        //stop climbing
        if (((Input.GetButtonUp("Fire2")) || (Input.GetKeyUp(KeyCode.LeftShift))) && climbing)
        {
            belowVine = false;
            almostFellVine = false;
            my_Rigidbody.gravityScale = 1;
            climbing = false;
        }
        if (wasClimbing && !almostFellVine)
        {
            climbing = false;
            //belowVine = false;
        }
        if (!belowVine && !touchingLatVine && !almostFellVine)
            climbing = false;

        if (aboveVine)
        {
            climbing = false;
            if (!chargedAttacking && !spearJumping)
            {
                my_Rigidbody.gravityScale = 1;
            }
        }
        if (!climbing)
        {
            belowVine = false;
            touchingLatVine = false;
            if (wasClimbing && !chargedAttacking && !spearJumping)
            {
                my_Rigidbody.gravityScale = 1;
                wasClimbing = false;
            }
        }
        //climb animations
        my_Animator.SetBool("Climbing", climbing && !prepAttack);
        my_Animator.SetBool("hanging", belowVine);
        my_Animator.SetBool("almostFell", almostFellVine);
        my_Animator.SetBool("lookingOut", lookingOutVine);

        //wall animation
        if (onWall && wallDirection.x < 0 && (Input.GetKeyUp(KeyCode.A)))
        {
            onWall = false;
        }
        if (onWall && wallDirection.x > 0 && (Input.GetKeyUp(KeyCode.D)))
        {
            onWall = false;
        }
        my_Animator.SetBool("ontoWall", onWall);

        //playercontrol
        if (playercontrol)
        {
            moveX = Input.GetAxis("Horizontal");
            moveY = Input.GetAxis("Vertical");

            if (moveX != 0 && !climbing)
            {
                Movement();
            }
            Jump();
        }
        spearMechanics();

        #region Direction

        //Direction

        //when aiming
        if (prepAttack || basicAttacking || chargedAttacking)
        {

            mousePos = Input.mousePosition;
            mousePos.z = -10f;

            playerPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            my_mouseDir.x = mousePos.x - playerPos.x;
            my_mouseDir.y = mousePos.y - playerPos.y;

            float angulo = (Mathf.Atan2(my_mouseDir.y, my_mouseDir.x) * Mathf.Rad2Deg) - 90;


            if (angulo < 0 && angulo > -160)
            {
                facingright = true;
            }
            if (angulo > 0 || angulo < -160)
            {
                facingright = false;
            }
            //flip
            if ((transform.localScale.x < 0 && facingright) || (transform.localScale.x > 0 && !facingright))
            {
                Flip();
            }
        }
        //when not aiming
        if (!prepAttack && !basicAttacking && !chargedAttacking && !touchingLatVine && !almostFellVine)
        {

            if (transform.localScale.x > 0)
            {
                facingright = true;
            }
            else
            {
                facingright = false;
            }

            if ((moveX < 0f && facingright) || (moveX > 0f && !facingright))
            {
                Flip();
            }

        }
    }

    #endregion

    void Flip()
    {
        facingright = !facingright;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }
    void Movement()
    {
        #region Walking


        if (onGround)
        {
            my_Rigidbody.velocity = new Vector2(moveX * walkSpeed, my_Rigidbody.velocity.y);

            if (!isJumping && my_Rigidbody.velocity.y > 0 && !inWater)
            {
                my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, 0);
            }
        }
        else
        {
            if (inWater)
            {
                my_Rigidbody.velocity = new Vector2(moveX * walkSpeed * 0.6f, my_Rigidbody.velocity.y);
            }
            //AirControl
            if (onAir && !spearJumping && !chargeEffect)
            {
                //aumentei aircontrol pra velocidade padrão
                my_Rigidbody.velocity = new Vector2(moveX * walkSpeed * 1, my_Rigidbody.velocity.y);
            }
            // momentum
            if (onAir && (spearJumping || chargeEffect))
            {
                if (my_Rigidbody.velocity.x > -5 && my_Rigidbody.velocity.x < 5)
                {
                    // se usar as habilidades com velocidade menor que a velocidade basica, o jogador pode adicionar.
                    my_Rigidbody.velocity = new Vector2(moveX * walkSpeed * 1, my_Rigidbody.velocity.y);
                    spearJumping = false;
                    chargeEffect = false;
                }
                // se usar as habilidades com velocidade maior que a basica, o jogador não modifica a velocidade naquela direção.
                if ((my_Rigidbody.velocity.x > 5 && moveX > 0) || (my_Rigidbody.velocity.x < -5 && moveX < 0))
                {
                    my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, my_Rigidbody.velocity.y);
                }
                // se durante o efeito da habilidade quiser ir na direção oposta, tudo normaliza.
                if ((my_Rigidbody.velocity.x > 0 && moveX < 0) || (my_Rigidbody.velocity.x < 0 && moveX > 0))
                {
                    spearJumping = false;
                    chargeEffect = false;
                }
            }
        }

        #endregion

    }

    void Jump()
    {
        if (!inWater)
        {
            // jump while not in water
            if (Input.GetButtonDown("Jump") && (onGround || climbing) && !belowVine)
            {
                isJumping = true;
                // i wanted to increase aircontrol without making the jump more powerful, so i nerfed the jump.
                my_Rigidbody.AddForce(new Vector2(0, baseJumpForce + runningJumpForce * Mathf.Abs(moveX * 0.1f)));

            }
            if (Input.GetButtonUp("Jump"))
            {
                isJumping = false;
            }
        }
        else
        {
            //Jump while in water
            if (Input.GetButton("Jump"))
            {
                isJumping = true;
                canAttack = true;
                canCharge = true;
            }
            if (Input.GetButtonUp("Jump"))
            {
                isJumping = false;
            }

        }
        //Height Control
        if (Input.GetButtonUp("Jump") && isJumping && my_Rigidbody.velocity.y > 0)
        {
            my_Rigidbody.velocity = new Vector3(my_Rigidbody.velocity.x, 0);
            isJumping = false;
        }
    }

    private void OnCollisionStay2D(Collision2D _col)
    {
        //wall
        if (_col.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            //not accelerating in walls by holding to their direction
            wallDirection.x = _col.transform.position.x - gameObject.transform.position.x;

            //wall to the right = no force to the right
            if (wallDirection.x > 0)
            {
                if ((moveX > 0.5 || my_Rigidbody.velocity.x > 0) && !wasClimbing && !climbing)
                {
                    if (!basicAttacking && !chargedAttacking && chargingTimer == 0)
                    {
                        onWall = true;
                    }
                    walkSpeed = 0;
                    if (my_Rigidbody.velocity.y < 0)
                    {
                        my_Rigidbody.velocity = new Vector2(0, my_Rigidbody.velocity.y);
                    }
                    if ((my_Rigidbody.velocity.y > 2) && !isJumping && !spearJumping && !chargedAttacking && !chargeEffect)
                    {
                        my_Rigidbody.velocity = new Vector2(0, -2);
                    }
                    if (isJumping && my_Rigidbody.velocity.y > 4.25f)
                    {
                        my_Rigidbody.velocity = new Vector2(0, -1);
                    }
                }
                if (moveX <= 0.5)
                {
                    walkSpeed = 5f;
                    onWall = false;
                }

            }
            //wall to the left = no force to the left
            if (wallDirection.x < 0)
            {
                if ((moveX < -0.5 || my_Rigidbody.velocity.x < 0) && !wasClimbing && !climbing)
                {
                    if (!basicAttacking && !chargedAttacking && chargingTimer == 0)
                    {
                        onWall = true;
                    }
                    walkSpeed = 0;
                    if (my_Rigidbody.velocity.y < 0)
                    {
                        my_Rigidbody.velocity = new Vector2(0, my_Rigidbody.velocity.y);
                    }
                    if ((my_Rigidbody.velocity.y > 2) && !isJumping && !spearJumping && !chargedAttacking && !chargeEffect)
                    {
                        my_Rigidbody.velocity = new Vector2(0, -2);
                    }
                    if (isJumping && my_Rigidbody.velocity.y > 4.25f)
                    {
                        my_Rigidbody.velocity = new Vector2(0, -1);
                    }
                }
                if (moveX >= -0.5f)
                {
                    walkSpeed = 5f;
                    onWall = false;
                }
            }
        }
    }
    private void OnCollisionExit2D(Collision2D _col)
    {
        //wall
        if (_col.gameObject.layer == LayerMask.NameToLayer("Wall"))
        {
            onWall = false;
            walkSpeed = 5f;
        }
    }
    private void OnTriggerStay2D(Collider2D _col)
    {

        if (_col.gameObject.CompareTag("Water"))
        {
            inWater = true;
            onAir = false;
            playercontrol = true;
            Time.timeScale = 1;
            prepAttack = false;
            canCharge = false;
            canAttack = false;
            canSpearJump = false;
            spearanim.updateMode = AnimatorUpdateMode.Normal;
            isJumping = false;

            // mudei aqui o valor da gravity scale pra aumentar o empuxo da água.

            //Water push up
            my_Rigidbody.gravityScale = -0.9f;

            if (my_Rigidbody.velocity.y < -5)
            {
                my_Rigidbody.AddForce(new Vector2(0, 8));
            }
        }

        //lateral vine
        if (_col.gameObject.CompareTag("Vine"))
        {

            belowVine = head.GetComponent<Head>().belowVine;
            aboveVine = feet.GetComponent<Player_Feet>().aboveVine;
            wasClimbing = false;
            if (climbing)
            {
                touchingLatVine = true;
            }
            if (!aboveVine)
            {
                if ((Input.GetButton("Fire2")) || (Input.GetKey(KeyCode.LeftShift)))
                {
                    touchingLatVine = true;
                    climbing = true;
                }
            }
            if (!belowVine)
            {
                //pra dentro é a direita
                if ((transform.position.x - _col.transform.position.x) < 0)
                {
                    rightVine = true;
                }

                //pra dentro é a esquerda
                if ((transform.position.x - _col.transform.position.x) > 0)
                {
                    rightVine = false;
                }
            }
            // pulando pra fora à direita
            if (!rightVine && moveX > 0 && isJumping && climbing)
            {
                climbing = false;
                if (!chargedAttacking)
                {
                    my_Rigidbody.gravityScale = 1;
                }

            }
            // pulando pra fora à esquerda
            if (rightVine && moveX < 0 && isJumping && climbing)
            {
                climbing = false;
                if (!chargedAttacking)
                {
                    my_Rigidbody.gravityScale = 1;
                }
            }
        }
        //ceiling vine
        if (_col.gameObject.CompareTag("Vine FnC"))
        {
            aboveVine = feet.GetComponent<Player_Feet>().aboveVine;
            if (!almostFellVine)
            {
                belowVine = head.GetComponent<Head>().belowVine;
            }
            wasClimbing = false;
            if (!aboveVine && belowVine)
            {
                almostFellVine = false;
                if ((Input.GetButton("Fire2")) || (Input.GetKey(KeyCode.LeftShift)))
                {
                    climbing = true;
                }
            }

        }
    }
    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject.CompareTag("Water"))
        {
            if (my_Rigidbody.velocity.y < -4)
            {
                water_FX.Play();
            }
            if (my_Rigidbody.velocity.magnitude > 5)
            {
                // mudei aqui o valor do divisor para ficar mais smooth, a queda dele dentro d'agua estava ganhando força.
                my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, my_Rigidbody.velocity.y / (my_Rigidbody.velocity.magnitude / 5.9f));
            }
        }
        //Taking damage from spikes
        if (_col.gameObject.CompareTag(tag_Spikes))
        {
            thisSpike = _col.transform;
            StartCoroutine(Knockback());
            gameObject.GetComponent<Entity>().ReceiveDamage(1);
        }
        if (_col.gameObject.CompareTag("Vine FnC"))
        {
            almostFellVine = false;
        }
    }

    private void OnTriggerExit2D(Collider2D _col)
    {
        if (_col.gameObject.CompareTag(tag_Water))
        {
            inWater = false;
            my_Rigidbody.gravityScale = 1;
            if (!isJumping)
            {
                my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, my_Rigidbody.velocity.y / 3);
                my_Rigidbody.drag = 10;
            }
            else
            {
                my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, my_Rigidbody.velocity.y * 0.9f);
            }

            if (Input.GetKey(KeyCode.Space))
            {
                my_Rigidbody.drag = 0;
                my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, 0);
                my_Rigidbody.AddForce(new Vector2(0, baseJumpForce * 0.8f));
                spearanim.updateMode = AnimatorUpdateMode.AnimatePhysics;
                isJumping = true;
            }
            if (climbing)
            {
                canAttack = true;
                canCharge = true;
                my_Rigidbody.drag = 0;
            }
        }

        //qualquer vinha
        if (((_col.gameObject.CompareTag("Vine")) || (_col.gameObject.CompareTag("Vine FnC"))) && !climbing)
        {
            aboveVine = feet.GetComponent<Player_Feet>().aboveVine;

            if (aboveVine)
            {
                climbing = false;
                if (!spearJumping && !chargedAttacking && !belowVine)
                {
                    my_Rigidbody.gravityScale = 1;
                }
            }

        }
        //vinha lateral
        if (_col.CompareTag("Vine"))
        {
            touchingLatVine = false;
            lookingOutVine = false;

            if (!isJumping && !chargedAttacking && !belowVine && !spearJumping && climbing)
            {
                if (moveY > 0 && ((transform.position.y - _col.transform.position.y) > 0))
                {
                    my_Rigidbody.velocity = new Vector2(0, (moveY * (walkSpeed * 0.8f)));
                }
                else
                {
                    if (my_Rigidbody.gravityScale == 0 && !isJumping)
                        my_Rigidbody.velocity = new Vector2(0, 0);
                }
                if (moveY < 0)
                {
                    moveY = 0;
                }
            }
            //saindo por cima ou por baixo
            if (!belowVine && !spearJumping && !chargedAttacking && climbing)
            {
                climbing = false;
                my_Rigidbody.gravityScale = 1;

            }
            if (belowVine)
            {
                touchingLatVine = false;
            }
            else
            {
                wasClimbing = true;
            }
        }
        //vinha superior
        if (_col.CompareTag("Vine FnC") && climbing && !chargedAttacking && !aboveVine && belowVine)
        {
            belowVine = head.GetComponent<Head>().belowVine;
            if (!belowVine)
            {
                if (!isJumping)
                {
                    my_Rigidbody.gravityScale = 0;
                }
                if (!touchingLatVine)
                {
                    wasClimbing = true;
                    if (moveY > 0)
                    {
                        wasClimbing = false;
                        my_Rigidbody.velocity = new Vector2(0, 0);
                        //pra fora é a esquerda
                        if (((transform.position.x - _col.transform.position.x) < 0))
                        {
                            rightVine = true;
                            if (facingright)
                                Flip();
                        }
                        // pra fora é a direita
                        if (((transform.position.x - _col.transform.position.x) > 0))
                        {
                            rightVine = false;
                            if (!facingright)
                                Flip();
                        }
                        almostFellVine = true;
                        belowVine = true;
                    }
                    //pra fora é a esquerda
                    if (((transform.position.x - _col.transform.position.x) < 0) && moveX < 0)
                    {
                        wasClimbing = false;
                        my_Rigidbody.velocity = new Vector2(0, 0);
                        rightVine = true;
                        almostFellVine = true;
                        belowVine = true;
                    }
                    //pra fora é a direita
                    if (((transform.position.x - _col.transform.position.x) > 0) && moveX > 0)
                    {
                        wasClimbing = false;
                        my_Rigidbody.velocity = new Vector2(0, 0);
                        rightVine = false;
                        almostFellVine = true;
                        belowVine = true;
                    }
                }
                if (touchingLatVine)
                {
                    belowVine = false;
                }
            }

        }
    }

    IEnumerator Knockback()
    {
        if (!climbing)
        {
            my_Rigidbody.velocity = Vector2.zero;
            playercontrol = false;
        }
        if (isJumping)
            isJumping = false;
        my_Rigidbody.AddForce((transform.position - thisSpike.transform.position).normalized * 50f);
        yield return new WaitForSeconds(0.3f);
        playercontrol = true;
    }

    #region spear
    void spearMechanics()
    {

        //Spear

        //spear direction
        if (prepAttack || basicAttacking || chargedAttacking)
        {
            mousePos = Input.mousePosition;
            mousePos.z = 5.75f;

            spearPos = Camera.main.WorldToScreenPoint(spear.transform.position);
            spear_mouseDir.x = mousePos.x - spearPos.x;
            spear_mouseDir.y = mousePos.y - spearPos.y;

            angleSpear = (Mathf.Atan2(spear_mouseDir.y, spear_mouseDir.x) * Mathf.Rad2Deg) - 90f;
            spear.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angleSpear));
        }

        spearanim.SetBool("attackStance", !inWater && !isJumping && !climbing && !onWall && !prepAttack && (canAttack || canCharge));

        //prepare spear attack

        spearanim.SetBool("prepAttack", prepAttack && !inWater && (canAttack || canCharge));

        if (Input.GetButtonDown("Fire1"))
        {
            if (angleSpear < -116f && angleSpear > -250)
            {
                aimDown = true;
            }
            else
            {
                aimDown = false;
            }
            chargeSlowmo = true;
            spearThrowing = false;
        }

        if (Input.GetButton("Fire1") && (canAttack || canCharge) && !basicAttacking)
        {

            prepAttack = true;
            chargingTimer += Time.deltaTime;

            if (chargingTimer < 0.3f)
            {
                previousSpeedx = my_Rigidbody.velocity.x;
                previousSpeedy = my_Rigidbody.velocity.y;
            }

            // charge's slow time
            if (chargingTimer > 0.3f && chargingTimer < 3.5f && canCharge && !chargedAttacking)
            {
                canAttack = false;
                canSpearThrow = false;
                spearJumping = false;
                climbing = false;
                onWall = false;
                spear_mouseDir = spear_mouseDir.normalized;
                playercontrol = false;
                Time.timeScale = 0.5f;
                if (chargeSlowmo)
                {
                    if (previousSpeedy < 0)
                    {
                        my_Rigidbody.gravityScale = 0.05f;
                        my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x / 3f, my_Rigidbody.velocity.y / 5f);
                    }
                    if (previousSpeedy > 0)
                    {
                        my_Rigidbody.gravityScale = 0.7f;
                        my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x / 3f, previousSpeedy / 1.5f);
                        new WaitForSeconds(1f);
                        if (my_Rigidbody.velocity.y < 0)
                        {
                            my_Rigidbody.gravityScale = 0.05f;
                            my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, previousSpeedy / 5f);
                        }
                    }
                    chargeSlowmo = false;
                }
            }
            // charge timeframe is over
            if (chargingTimer > 3.5f)
            {
                my_Rigidbody.velocity = new Vector2(previousSpeedx, my_Rigidbody.velocity.y);
                if (previousSpeedy < 0)
                {
                    my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, previousSpeedy);
                }
                my_Rigidbody.gravityScale = 1;
                playercontrol = true;
                Time.timeScale += Time.deltaTime * 0.5f;
                if (Time.timeScale >= 1.3)
                    Time.timeScale = 1.3f;
                canCharge = false;
                chargingTimer = 0;
                prepAttack = false;
                canSpearThrow = true;
                new WaitForSeconds(0.3f);
                Time.timeScale = 1;
            }
        }
        //basic attack
        if (Input.GetButtonUp("Fire1") && (chargingTimer > 0f && chargingTimer < 0.3f && canAttack))
        {
            Time.timeScale = 1;
            my_Rigidbody.gravityScale = 1;
            if (!spearJumping)
            {
                playercontrol = true;
            }
            onWall = false;
            basicAttacking = true;
            chargedAttacking = false;
            prepAttack = false;
            chargingTimer = 0;
            chargeSlowmo = false;
            StartCoroutine(SpearAttack());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            prepAttack = false;
        }

        //charged attack
        if (Input.GetButtonUp("Fire1") && chargingTimer > 0.3f && canCharge)
        {
            chargedAttacking = true;
            chargingTimer = 0;
            chargeSlowmo = false;
            basicAttacking = false;
            prepAttack = false;
            spearJumping = false;
            onWall = false;

            StartCoroutine(ChargedAttack());
        }

        if (!canCharge && chargingTimer > 0.3f)
        {
            chargingTimer = 0;
        }
        //spear throw
        /*
        pullingSpear = spear.GetComponent<Spear>().spearPulled;

        if (Input.GetButton("Fire2") && holdingSpear && !basicAttacking && canSpearThrow && !pullingSpear)
        {
            prepAttack = true;
            if (!basicAttacking || !chargedAttacking || !spearJumping || !inWater)
            {
                spear_col.isTrigger = true;
                spearThrowing = true;
                chargingTimer = 0.1f;
            }

        }
        if (Input.GetButtonUp("Fire2") && spearThrowing && canSpearThrow)
        {

            prepAttack = false;
            canSpearThrow = false;
            chargingTimer = 0;
            spearanim.enabled = false;
            spear_Rigidbody.freezeRotation = true;
            StartCoroutine(spearThrow());

        }
        */
    }

    IEnumerator SpearAttack()
    {
        spearanim.SetTrigger("AttackButton");
        yield return new WaitForSeconds(0.125f);
        basicAttacking = false;
        canSpearThrow = true;

        //spear jump
        if (canSpearJump && onGround && aimDown)
        {
            spearJumping = true;
            canSpearThrow = false;
            canSpearJump = false;
            chargeEffect = false;
            prepAttack = false;
            atkGround = false;
            canCharge = false;
            canAttack = false;
            my_Rigidbody.gravityScale = 0;
            playercontrol = false;

            my_Rigidbody.AddForce(new Vector2(moveX * spearJumpx, spearJumpy));
            iTween.ShakeScale(mesh, Vector3.one * 0.05f, 0.05f);
            yield return new WaitForSeconds(0.15f);
            canAttack = true;
            canCharge = true;
            canSpearThrow = true;
            yield return new WaitForSeconds(0.1f);
            playercontrol = true;
            yield return new WaitForSeconds(0.1f);
            my_Rigidbody.gravityScale = 1;
            if (onGround)
                spearJumping = false;
            yield return new WaitForSeconds(1.5f);
            spearJumping = false;
        }
    }

    IEnumerator ChargedAttack()
    {
        Time.timeScale = 1;
        spearanim.SetTrigger("AttackButton");
        playercontrol = false;
        previousSpeedx = previousSpeedx * 0.3f;
        previousSpeedy = previousSpeedy * 0.3f;
        my_Rigidbody.gravityScale = 0;
        canAttack = false;

        //não ter a força anulada
        if ((previousSpeedx > 0 && my_mouseDir.x < 0) || (previousSpeedx < 0 && my_mouseDir.x > 0))
        {
            my_Rigidbody.velocity = new Vector2(0, my_Rigidbody.velocity.y);
        }
        if ((previousSpeedy > 0 && my_mouseDir.y < 0) || (previousSpeedy < 0 && my_mouseDir.y > 0))
        {
            my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, 0);
        }

        // manter o flow se o charge for na mesma direção
        if ((previousSpeedx > 0 && my_mouseDir.x > 0) || (previousSpeedx < 0 && my_mouseDir.x < 0))
        {
            my_Rigidbody.velocity = new Vector2(previousSpeedx, my_Rigidbody.velocity.y);
        }
        if ((previousSpeedy > 0 && my_mouseDir.y > 0) || (previousSpeedy < 0 && my_mouseDir.y < 0))
        {
            my_Rigidbody.velocity = new Vector2(my_Rigidbody.velocity.x, previousSpeedy);
        }
        my_Rigidbody.AddForce(my_mouseDir.normalized * chargePower);
        iTween.ShakeScale(mesh, Vector3.one * 0.1f, 0.2f);


        yield return new WaitForSeconds(0.25f);
        chargedAttacking = false;
        if (onAir)
            canCharge = false;
        playercontrol = true;
        chargeEffect = true;
        canAttack = true;
        canSpearThrow = true;
        yield return new WaitForSeconds(0.1f);
        my_Rigidbody.gravityScale = 1;
        yield return new WaitForSeconds(1f);
        chargeEffect = false;
    }

    IEnumerator harpoonThrow()
    {
        spearanim.enabled = false;
        canAttack = false;
        canSpearJump = false;
        spear_Rigidbody.bodyType = RigidbodyType2D.Dynamic;
        spear_Rigidbody.AddForce(spear_mouseDir.normalized * 500f);
        spear_Rigidbody.gravityScale = 1;
        spearThrowing = false;
        spear_col.enabled = true;
        spear_col.isTrigger = true;
        holdingSpear = false;
        yield break;
    }
    #endregion
}
