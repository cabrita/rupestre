﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floating : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D this_Rigidb;
    public ParticleSystem water_FX;
    void Start()
    {
        this_Rigidb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerStay2D(Collider2D _col)
    {

        if (_col.gameObject.CompareTag("Water"))
        {
            this_Rigidb.gravityScale = -0.9f * this_Rigidb.mass;

            if (this_Rigidb.velocity.y < -5)
            {
                this_Rigidb.AddForce(new Vector2(0, 8 * this_Rigidb.mass));
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject.CompareTag("Water"))
        {
            if (this_Rigidb.velocity.magnitude > 3)
            {
                water_FX.Play();
                this_Rigidb.velocity = new Vector2(this_Rigidb.velocity.x, this_Rigidb.velocity.y / (this_Rigidb.velocity.magnitude / (6f * this_Rigidb.mass)));
            }
        }
    }
    private void OnTriggerExit2D(Collider2D _col)
    {
        this_Rigidb.gravityScale = 1;
        if (this_Rigidb.mass < 1)
        {
            this_Rigidb.velocity = new Vector2(this_Rigidb.velocity.x / (this_Rigidb.mass * 3), this_Rigidb.velocity.y / (this_Rigidb.mass * 5));
        }
        else
        {
            this_Rigidb.velocity = new Vector2(this_Rigidb.velocity.x / (3 / this_Rigidb.mass), this_Rigidb.velocity.y / (5 / this_Rigidb.mass));
        }
    }
}
