﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Entity : MonoBehaviour
{
    public int hp_Maximum;
    protected int hp_Current;

    public float sp_Maximum;
    protected float sp_Current;


    public bool immortal;
    private bool isAlive { get { return (hp_Current > 0); } }

    public UnityEvent DeathEvent;
    public UnityEvent DamageEvent;


    public virtual void Awake()
    {
        hp_Current = hp_Maximum;
        sp_Current = sp_Maximum;
    }

    public virtual void ReceiveDamage(int _damage)
    {
        if (immortal || !isAlive)
        {
            return;
        }
        hp_Current -= _damage;
        OnDamage();

        if (!isAlive)
        {
            hp_Current = 0;
            OnDeath();
        }
    }

    public float CheckSP()
    {
        return sp_Current;
    }

    public virtual void RecieveSP(float addition)
    {
        if (immortal || !isAlive)
        {
            return;
        }
        if (addition > 0)
        {
            if (sp_Current >= sp_Maximum)
            {
                sp_Current = sp_Maximum;
                return;
            }
            else
            {
                sp_Current += addition;
                if (sp_Current > sp_Maximum)
                {
                    sp_Current = sp_Maximum;
                    return;
                }
            }
        }
        if (addition < 0)
        {
            if (sp_Current <= 0)
            {
                return;
            }
            else
            {
                sp_Current += addition;
                if (sp_Current < 0)
                {
                    sp_Current = 0;
                }
            }
        }
    }

    public virtual void Heal(int _ammount)
    {
        hp_Current += _ammount;
        if (hp_Current > hp_Maximum)
        {
            hp_Current = hp_Maximum;
        }
    }

    public virtual void OnDamage()
    {
        //iTween.PunchScale(gameObject, Vector3.one / 3, 0.5f);
        DamageEvent.Invoke();
    }

    public virtual void OnDeath()
    {
        DeathEvent.Invoke();
    }
}
