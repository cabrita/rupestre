﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Stats : Entity
{
    private static Player_Stats instance;
    public static Player_Stats Instance { get { return instance; } }

    int lifePieces;
    public ParticleSystem hp_Up_FX;


    public ParticleSystem item_FX;
    public ParticleSystem healing_FX;
    public ParticleSystem hitted_FX;




    public override void Awake()
    {
        base.Awake();
        instance = this;
    }

    public void AddLifePiece()
    {
        lifePieces++;
        if (lifePieces == 4)
        {
            lifePieces = 0;
            hp_Maximum++;
            hp_Up_FX.Play();
            Heal();
        }
    }

    public override void ReceiveDamage(int _damage)
    {
        base.ReceiveDamage(_damage);
        hitted_FX.Play();
    }

    public void Heal()
    {
        hp_Current = hp_Maximum;
    }

    public override void Heal(int _ammount)
    {
        base.Heal(_ammount);
        healing_FX.Play();
    }
}
