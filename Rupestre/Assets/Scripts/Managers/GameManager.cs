﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject pauseMenu;
    public GameObject pauseCam;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CallMenu();
        }
    }

    public void CallMenu()
    {
        if (pauseMenu.activeInHierarchy)
        {
            ResumeGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        //Time.timeScale = 0;
        pauseMenu.SetActive(true);
        pauseCam.SetActive(true);
    }

    public void ResumeGame()
    {
        //Time.timeScale = 1;
        pauseMenu.SetActive(false);
        pauseCam.SetActive(false);
    }
}
