﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : MonoBehaviour
{
    public bool spearGround;
    [SerializeField] private bool spearAttached;
    public bool spearStuck;
    public bool spearPulled;

    public GameObject player;
    private Rigidbody2D spear_Rigidbody;
    private Animator spear_anim;
    public BoxCollider2D spear_col;
    public CapsuleCollider2D speartip_col;

    Vector2 playerPos;
    Vector2 playerDir;
    Vector3 spearPos;

    // Start is called before the first frame update
    void Start()
    {
        spear_Rigidbody = gameObject.GetComponent<Rigidbody2D>();
        spear_anim = gameObject.GetComponent<Animator>();
        spear_col = gameObject.GetComponent<BoxCollider2D>();
        speartip_col = gameObject.GetComponent<CapsuleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        spearAttached = player.GetComponent<Player_Movement>().holdingSpear;
        if (!spearAttached)
        {
            DetachfromParent();
            spearPos = Camera.main.WorldToScreenPoint(gameObject.transform.position);
            playerPos = Camera.main.WorldToScreenPoint(player.transform.position);
            playerDir.x = playerPos.x - spearPos.x;
            playerDir.y = playerPos.y - spearPos.y;

            if (spear_Rigidbody.velocity.y < 0f && !spearStuck && !spearPulled)
            {
                spear_Rigidbody.freezeRotation = false;
            }
            if (!spearPulled && (Input.GetButtonDown("Fire2")))
            {
                spearStuck = false;
                spear_Rigidbody.bodyType = RigidbodyType2D.Dynamic;
                spearPulled = true;
            }
            if (spearPulled)
            {
                spear_col.isTrigger = true;
                spear_Rigidbody.bodyType = RigidbodyType2D.Dynamic;
                spear_Rigidbody.AddForce(playerDir.normalized * 50f);
            }

        }
        if (spearAttached)
        {
            transform.parent = player.transform;
            spear_Rigidbody.velocity = new Vector2(0, 0);
            spear_col.enabled = false;
            spear_anim.enabled = true;
            spear_anim.updateMode = AnimatorUpdateMode.AnimatePhysics;
            spear_Rigidbody.angularVelocity = 0;
            spear_Rigidbody.gravityScale = 0;
            spearStuck = false;
            spearPulled = false;
        }
    }
    void DetachfromParent()
    {
        transform.parent = null;
    }
    void OnTriggerStay2D(Collider2D _col)
    {
        if (_col.gameObject.layer == LayerMask.NameToLayer("Ground") && spearAttached && !spearPulled)
        {
            spearGround = true;
            Debug.Log("atkGround");
        }
        //spear after thrown
        if (_col.gameObject.layer == LayerMask.NameToLayer("Ground") && !spearAttached && !spearPulled)
        {
            spear_Rigidbody.bodyType = RigidbodyType2D.Static;
            spearStuck = true;
        }
        if (_col.gameObject.CompareTag("Wall") && !spearAttached && !spearPulled)
        {
            spear_Rigidbody.bodyType = RigidbodyType2D.Static;
            spearStuck = true;
        }
        if (_col.gameObject.CompareTag("Ceiling") && !spearAttached)
        {
            spear_Rigidbody.bodyType = RigidbodyType2D.Static;
            spearStuck = true;
        }

    }
    void OnTriggerEnter2D(Collider2D _col)
    {
        if (_col.gameObject.CompareTag("Player") && spearPulled)
        {
            spear_Rigidbody.velocity = new Vector2(0, 0);
            spear_anim.updateMode = AnimatorUpdateMode.Normal;
            spear_Rigidbody.freezeRotation = false;
            spear_Rigidbody.rotation = 0;
        }
    }
    void OnTriggerExit2D(Collider2D _col)
    {
        if (_col.gameObject.layer == LayerMask.NameToLayer("Ground") && spearAttached)
        {
            spearGround = false;
        }
        if (_col.gameObject.CompareTag("Player") && !spearAttached)
        {
            spear_col.isTrigger = false;
        }
    }

}
